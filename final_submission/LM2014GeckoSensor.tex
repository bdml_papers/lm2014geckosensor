% Adhesive Stress Distribution Measurement on a Gecko
% Eric V. Eason, Elliot W. Hawkes, Marc Windheim, David L. Christensen, Thomas Libby, and Mark R. Cutkosky
% Extended abstract for Living Machines 2014
% 7 May 2014

\documentclass[runningheads,a4paper]{llncs}

\usepackage{color}
\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{url}
\usepackage{times}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}

\title{Adhesive Stress Distribution Measurement on a Gecko}

\author{Eric~V.~Eason\inst{1} \and Elliot~W.~Hawkes\inst{2} \and Marc~Windheim\inst{3} \and David~L.~Christensen\inst{2} \and Thomas~Libby\inst{4} \and Mark~R.~Cutkosky\inst{2}}

\authorrunning{E. V. Eason et al.}

\institute{Dept. of Applied Physics, Stanford University, Stanford, CA 94305, USA
	\and  Dept. of Mechanical Engineering, Stanford University, Stanford, CA 94305, USA
	\and Technische Universit{\"a}t M{\"u}nchen, 85748 Garching, Germany
	\and CiBER, University of California, Berkeley, CA 94720, USA \\
	\email{easone@stanford.edu}}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{abstract}
	Gecko adhesion has inspired climbing robots and synthetic adhesive grippers. Distributing loads between patches of adhesive is important for maximum performance in gecko-inspired devices, but it is unknown how the gecko distributes loads over its toes. We report \emph{in vivo} measurements of stress distributions on gecko toes. The results are significantly non-uniform.
\end{abstract}

	Gecko-inspired adhesives are seeing use in applications ranging from climbing robots \cite{Hawkes2013,Hawkes2013b} to orbital debris capture \cite{Parness2013}. As adhesive systems grow larger and more sophisticated, with multiple independent patches of adhesive, it becomes clear that they must be designed holistically: the mechanisms that apply and distribute loads are as important as the adhesive itself. For example, the gecko-inspired gripper shown in Fig.~\ref{fig:gripper} uses an array of tiles of adhesive, each loaded with a tendon to prevent moments over the tiles, and with load-sharing mechanisms to achieve uniform forces over the entire array \cite{Parness2013}, \cite{Hawkes2013}. Load-sharing increases the overall performance by preventing individual tiles from becoming overloaded.

\begin{figure}[b]
	\centering
	\includegraphics{Fig01.pdf}
	\caption{Prototype of gecko-inspired adhesive gripper for retrieving space debris \cite{Parness2013}. Reprinted by permission of the American Institute of Aeronautics and Astronautics, Inc.}
	\label{fig:gripper}
\end{figure}

	We desire to compare these load-sharing mechanisms with the equivalent parts of the gecko adhesive system. Geckos make use of multiple patches of adhesive, but it is not known to what extent they achieve load-sharing between patches. The toes of the gecko are divided into flaps of skin called lamellae (or scansors), which are in turn covered in arrays of microscopic adhesive setae. The lamellae are loaded independently by a branched system of tendons. In this natural adhesion system, as in synthetic designs, the distribution of adhesive stresses within and between lamellae determines the overall system performance. It is possible that stretching of the lamellar skin or sliding of lamellae relative to each other could contribute to the redistribution of loads. 

	There is some evidence that the gecko adhesive system may not be ideal. In the tokay gecko (\emph{Gekko gecko}), measurements at different scales of adhesive area show that there is over a 50-fold decrease in adhesive shear stress as the area increases from a single seta to a whole animal \cite{Autumn2006}. It has been suggested that this decrease in performance as area increases is explained by a reduced contact area or unequal load-sharing between or within lamellae \cite{Autumn2006,BullockFederle2011}.

	To investigate this question further, we conducted the first known \emph{in vivo} measurements of stress distribution and contact area on tokay gecko toes, using a custom optical tactile sensor optimized for spatial and temporal resolution (100~$\mu$m, 60~Hz) rather than stress accuracy. The sensor converts compressive or tensile normal stress into a light signal that is captured by a video camera. It uses frustrated total internal reflection (FTIR) within an acrylic waveguide covered by an 80~$\mu$m thick  microtextured PDMS film (Fig.~\ref{fig:sensor}), molded from an etched Si wafer with pyramidal pits. Light enters the waveguide and is internally reflected until it scatters at points where the waveguide is in contact with the film \cite{Begej1988}. Higher pressures cause the scattering to increase as the microtexture is compressed. A vacuum between the film and waveguide adds a compressive stress offset and allows the sensor to measure tensile (adhesive) stresses.

	The sensor was oriented vertically, and an adult tokay gecko of body mass 101.2~g was allowed to attach one of its hind feet to the sensor and hang, head downwards. The gecko was then pulled off the sensor manually. This procedure was repeated multiple times and the pull angle was varied to obtain different loading configurations on the foot. Selected test results are shown in Fig.~\ref{fig:toeresults}.

	For measurements of contact area, the microtextured PDMS film was removed and the gecko was allowed to attach to the bare acrylic waveguide (Fig.~\ref{fig:toeresults}a). It is apparent that a significant fraction of the setal area does not make contact. The most proximal lamellae are totally off the surface, but even for lamellae that touch the surface there are non-contacting regions that appear wrinkled or folded.

\begin{figure}
	\centering
	\includegraphics{Fig02.pdf}
	\caption{FTIR-based optical tactile sensor for normal stress distribution measurements. A PDMS film with pyramidal bumps (29~$\mu$m height, 54~$\mu$m width, 100~$\mu$m spacing) produces a scattered light image where the light intensity is proportional to the normal stress.}
	\label{fig:sensor}
\end{figure}

\begin{figure}
	\centering
	\includegraphics{Fig03.pdf}
	\caption{Third toe of right hind limb of a tokay gecko under varying loading conditions. (a)~The toe is placed on an acrylic waveguide illuminated by total internal reflection. The real area of contact is indicated by the white regions. (b)~The toe is placed on the custom tactile sensor. Light yellow regions indicate compressive normal stress, and dark blue regions indicate tensile normal stress.}
	\label{fig:toeresults}
\end{figure}

	There are also variations in normal stress across the toe (Fig.~\ref{fig:toeresults}b). The highest tensile stresses are limited to a small fraction of the total area of the adhesive. The locations of the force concentrations vary between tests and depend on the angle of the external load. The data also suggest that some lamellae were under compressive stress.

	From these results, the tokay gecko does not achieve full contact of its adhesive to a flat surface, and within the portion of the adhesive that does make contact, there is a nonuniform adhesive stress distribution. This result is consistent with observations of decreasing adhesive performance with increasing area, and demonstrates that the adhesive system of the tokay gecko is non-ideal at the toe scale. The tokay can nevertheless support many times its body weight, so full contact area and equal load-sharing are less critical for geckos than for current synthetic systems.

\vspace{-0.3cm}

\subsubsection*{Acknowledgments.} This work was supported by the NSF Graduate Research Fellowship Program, the Stanford Graduate Fellowship Program, and the Hertz Foundation.

\vspace{-0.3cm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{thebibliography}{1}

\bibitem{Hawkes2013}
Hawkes, E., Eason, E., Asbeck, A., Cutkosky, M.: The gecko's toe: scaling directional adhesives for climbing applications. IEEE/ASME Trans. Mechatron. 18, 518--526 (2013)

\bibitem{Hawkes2013b}
Hawkes, E., et al.: Dynamic surface grasping with directional adhesion. In: 2013 IEEE/RSJ Int. Conf. Intelligent Robots and Systems (IROS), pp. 5487--5493 (2013)

\bibitem{Parness2013}
Parness, A., et al.: On-off adhesive grippers for Earth-orbit. In: AIAA SPACE 2013 Conf. and Exposition (2013)

\bibitem{Autumn2006}
Autumn, K.: Properties, principles, and parameters of the gecko adhesive system. In: Smith, A.M., Callow, J.A. (eds.) Biological Adhesives, pp. 225-256. Springer, Berlin (2006)

\bibitem{BullockFederle2011}
Bullock, J., Federle, W.: Beetle adhesive hairs differ in stiffness and stickiness: in vivo adhesion measurements on individual setae. Naturwissenschaften 98, 381--387 (2011)

\bibitem{Begej1988}
Begej, S.: Planar and finger-shaped optical tactile sensors for robotic applications. IEEE J. Robot. Autom. 4, 472--484 (1988)

\end{thebibliography}

\end{document}

